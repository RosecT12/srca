__author__ = 'Talita Rosec'

from random import shuffle
from tkinter import *
import time

## KARTE: C=clubs (♣), D=diamonds (♦), H=hearts (♥), S=spades (♠) ##

class Karta:
    def __init__(self,barva,rank,oznaka=None):
        self.barva=barva
        self.rank=rank
        self.oznaka=oznaka

        #kartam damo vrednosti glede na rank
        if self.rank in ['2','3','4','5','6','7','8','9']:
            self.vrednost_rank=int(self.rank)
        elif self.rank=='T':
            self.vrednost_rank=10
        elif self.rank=='J':
            self.vrednost_rank=11
        elif self.rank=='Q':
            self.vrednost_rank=12
        elif self.rank=='K':
            self.vrednost_rank=13
        elif self.rank=='A':
            self.vrednost_rank=14

        #da so karte razdeljene križ-kara-pik-srca
        if self.barva=='C':
            self.vrednost_barva=1
        elif self.barva=='D':
            self.vrednost_barva=2
        elif self.barva=='S':
            self.vrednost_barva=3
        elif self.barva=='H':
            self.vrednost_barva=4

    #karte izpiše v obliki 'barva rank'
    def __str__(self):
        return (self.barva + ' ' + self.rank)

    def __eq__(self,other):
        return (self.barva,self.rank)==(other.barva,other.rank)

    def __lt__(self,other):
        (self.barva,self.vrednost_rank)<(other.barva,other.vrednost_rank)

    def narisi_karto(self,platno,karta,x,y):
        self.slika=karta
        self.oznaka=platno.create_image(x,y,image=karta)

    def zbrisi_karto(self,platno):
        platno.delete(self.oznaka)



class Deck:
    def __init__(self, shuffled=True):
        self.karte=[]

        barva = ['C','D','S','H']
        rank = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
        for b in barva:
            for r in rank:
                self.karte.append(Karta(b,r))

    def premesaj(self):
        shuffle(self.karte)

    def uredi(self):
        self.karte.sort(key=lambda karta:(karta.vrednost_barva, karta.vrednost_rank))
        return self.karte



class Igralec():
    def __init__(self,igra,ime=''):

        self.igra=igra #podatki o igri iz razreda Srca

        self.karte=[] #igralčeve karte
        self.tocke1=0 #točke po vsakem krogu (vsak igralec da eno karto na kup)
        self.tocke=0 #točke po eni celi rundi (odloženih vseh 13 kart)
        self.ime=ime
        
        self.nacin_igre=None #True-klatimo zvezde, False-želimo pobrati čim manj

        self.odlozene=[] #karte, ki jih igralec odloži v eni celi rundi (odloženih vseh 13kart)


        #igralcem nastavimo številke
        if self.ime=='Jug':
            self.stevilka=0
        elif self.ime=='Zahod':
            self.stevilka=1
        elif self.ime=='Sever':
            self.stevilka=2
        elif self.ime=='Vzhod':
            self.stevilka=3

        self.deck=Deck()


    def dodaj_karto(self,karta):
        self.karte.append(karta)

    # preveri, če ima igralec križevo dvojko
    def ima_krizevo_dvojko(self):
        for karta in self.karte:
            if (karta.barva,karta.rank)==('C','2'):
                return True
            else:
                return False

    #nisem zadnji, ki daje karto na kup - želim pobrati(klatim zvezde)
    def poberi(moje_karte,na_kupu):
        vrednosti_mojih=[] # seznam vrednosti mojih kart
        vrednosti_kupa=[] # seznam vrednosti kart na kupu
        for karta in moje_karte:
            vrednosti_mojih.append(karta.vrednost_rank)
        for karta in na_kupu:
            vrednosti_kupa.append(karta.vrednost_rank)
        max_vr_m=max(vrednosti_mojih)
        min_vr_m=min(vrednosti_mojih)
        max_vr_k=max(vrednosti_kupa)
        #če je moja največja karta manjša od največje karte na kupu
        #ne bom pobral v nobenem primeru
        #znebim se svoje najmanjše
        if max_vr_m<max_vr_k:
            return moje_karte[vrednosti_mojih.index(min_vr_m)]
        #dam svojo največjo karto, upam, da bom pa le morda pobral
        else:
            return moje_karte[vrednosti_mojih.index(max_vr_m)]

    #sem zadnji, ki daje karto na kup - želim pobrati(klatim zvezde)
    def poberi_zadnji(moje_karte,na_kupu):
        vrednosti_mojih=[]
        vrednosti_kupa=[]
        for karta in moje_karte:
            vrednosti_mojih.append(karta.vrednost_rank)
        for karta in na_kupu:
            vrednosti_kupa.append(karta.vrednost_rank)
        min_vr_m=min(vrednosti_mojih) #najmanjša *vrednost* mojih kart
        max_vr_m=max(vrednosti_mojih)
        min_vr_mojih=moje_karte[vrednosti_mojih.index(min_vr_m)] #moja *karta* manimalne vrednosti
        max_vr_mojih=moje_karte[vrednosti_mojih.index(max_vr_m)]
        max_vr_k=max(vrednosti_kupa) #največja *vrednost* karte na kupu
        max_vr_kupa=na_kupu[vrednosti_kupa.index(max_vr_k)] #*karta* maksimalne vrednosti kart na kupu
        if max_vr_m < max_vr_k: #če je moja največja karta manjše vrednosti od karte največje vrednosti na kupu
            return min_vr_mojih #izberem karto najmanjše vrednosti (da mi ne grenijo življenja ko skušam sklatiti zvezde)
        else:
            i=len(moje_karte)-1
            while moje_karte[i].vrednost_rank > max_vr_kupa.vrednost_rank:
                if i > 0:
                    i-=1
                else:
                    break
            if moje_karte[i].vrednost_rank < max_vr_kupa.vrednost_rank:
                return moje_karte[i+1]
            return moje_karte[i]

    #nisem zadnji, ki daje karto na kup - ne želim pobrati
    def izberi_najmanjso(moje_karte,na_kupu):
        vrednosti_mojih=[]
        vrednosti_kupa=[]
        for karta in moje_karte:
            vrednosti_mojih.append(karta.vrednost_rank)
        for karta in na_kupu:
            vrednosti_kupa.append(karta.vrednost_rank)
        min_vr_m=min(vrednosti_mojih) #najmanjša *vrednost* mojih kart
        min_vr_mojih=moje_karte[vrednosti_mojih.index(min_vr_m)] #moja *karta* manimalne vrednosti
        max_vr_k=max(vrednosti_kupa) #največja *vrednost* karte na kupu
        max_vr_kupa=na_kupu[vrednosti_kupa.index(max_vr_k)] #*karta* maksimalne vrednosti kart na kupu
        if min_vr_m > max_vr_k: #če je moja najmanjša karta večje vrednosti od karte največje vrednosti na kupu
            return min_vr_mojih #izberem karto najmanjše vrednosti (nisem zadnji, upam da bo kdo za mano imel karto večje vrednosti)
        else:
            i=0
            while moje_karte[i].vrednost_rank < max_vr_kupa.vrednost_rank:
                if i < len(moje_karte)-1:
                    i+=1
                else:
                    break
            if moje_karte[i].vrednost_rank > max_vr_kupa.vrednost_rank:
                return moje_karte[i-1]
            return moje_karte[i]

    #sem zadnji, ki daje karto na kup - ne želim pobrati
    def izberi_najmanjso_zadnji(moje_karte,na_kupu):
        vrednosti_mojih=[] # seznam vrednosti mojih kart
        vrednosti_kupa=[] # seznam vrednosti kart na kupu
        for karta in moje_karte:
            vrednosti_mojih.append(karta.vrednost_rank)
        for karta in na_kupu:
            vrednosti_kupa.append(karta.vrednost_rank)
        max_vr_kupa=na_kupu[vrednosti_kupa.index(max(vrednosti_kupa))] #karta maksimalne vrednosti kart na kupu
        #dam karto največje možne vrednosti, ki ne bo pobrala kupa
        #če take karte nimam, dam karto največje možne vrednosti ustrezne barve
        #uporabno, ker sem *zadnji* na vrsti
        i=0
        while moje_karte[i].vrednost_rank < max_vr_kupa.vrednost_rank:
            if i < len(moje_karte)-1:
                i+=1
            else:
                break
        if moje_karte[i].vrednost_rank > max_vr_kupa.vrednost_rank:
            return moje_karte[i-1]
        return moje_karte[i]


class Racunalnik(Igralec):
    def __init__(self,igra,ime=''):
        Igralec.__init__(self,igra,ime)

    def poberi_cim_manj(self,prva_runda=True):
        ustrezne_karte=[]
        nenegativne_karte=[]
        vr_neneg=[]
        ustrezne_odlozene=[]
        vrednosti=[]
        
        # vržene bodo prve karte - vsi igralci imajo še vseh 13 kart
        if len(self.karte)==13:
            for karta in self.igra.kup:
                if karta.barva=='C':
                    ustrezne_odlozene.append(karta)
            for karta in self.karte:
                if karta.barva!='H' and (karta.barva,karta.rank)!=('S','Q'):
                    nenegativne_karte.append(karta)
                if karta.barva=='C':
                    ustrezne_karte.append(karta)
                #začenem
                if (karta.barva,karta.rank)==('C','2'):
                    krizeva_dvojka=karta
                    self.izbrana_karta=krizeva_dvojka
                #ne začnem
                if not self.ima_krizevo_dvojko():
                    #če kart zahtevane barve nimamo
                    if len(ustrezne_karte)==0:
                        #v prvem krogu ne smemo metati kart, ki dajejo
                        #negativne točke
                        for karta in nenegativne_karte:
                            vr_neneg.append(karta.vrednost_rank)
                        max_vr=max(vr_neneg)
                        for karta in nenegativne_karte:
                            #znebimo se karte največje vrednosti,
                            #ki ne daje negativnih točk
                            if karta.vrednost_rank==max_vr:
                                self.izbrana_karta=karta
                    #imamo vsaj eno karto zahtevane barve

                    if len(ustrezne_karte)>0:
                        if len(self.igra.kup)==3:
                            self.izbrana_karta=Igralec.izberi_najmanjso_zadnji(ustrezne_karte,ustrezne_odlozene)
                        if len(self.igra.kup)<3:
                            self.izbrana_karta=Igralec.izberi_najmanjso(ustrezne_karte,ustrezne_odlozene)

            return self.izbrana_karta


        # prve karte se že bile odvržene - vsi igralci imajo 12 kart ali manj
        else:
            #krog začnem jaz (sem pobral kup prejšnji krog)
            if self.igra.kup == []:
                for karta in self.karte:
                    vrednosti.append(karta.vrednost_rank)
                #dam karto najmanjše vrednosti (upam, da ne bom pobral)
                min_vr=min(vrednosti)
                for karta in self.karte:
                    if karta.vrednost_rank==min_vr:
                        self.izbrana_karta=karta

            else: #jaz ne začnem kroga
                prva_karta=self.igra.kup[0]
                for karta in self.igra.kup:
                    if karta.barva==prva_karta.barva:
                        ustrezne_odlozene.append(karta)
                for karta in self.karte:
                    vrednosti.append(karta.vrednost_rank)
                    if karta.barva==prva_karta.barva:
                        ustrezne_karte.append(karta)
                if len(ustrezne_karte)==0:
                    max_vr=max(vrednosti)
                    for karta in self.karte:
                        #znebimo se karte največje vrednosti
                        if karta.vrednost_rank==max_vr:
                            self.izbrana_karta=karta
                #imamo vsaj eno karto zahtevane barve
                if len(ustrezne_karte)>0:
                    if len(self.igra.kup)==3:
                        self.izbrana_karta=Igralec.izberi_najmanjso_zadnji(ustrezne_karte,ustrezne_odlozene)
                    if len(self.igra.kup)!=3:
                        self.izbrana_karta=Igralec.izberi_najmanjso(ustrezne_karte,ustrezne_odlozene)

            return self.izbrana_karta

    def sklati_zvezde(self,prva_runda=True):
        ustrezne_karte=[]
        nenegativne_karte=[]
        vr_neneg=[]
        ustrezne_odlozene=[]
        ustrezne_vr=[]
        nesrceve_karte=[]
        nesrceve_vr=[]
        vrednosti=[]
        
        if len(self.karte)==13:
            for karta in self.igra.kup:
                if karta.barva=='C':
                    ustrezne_odlozene.append(karta)
            for karta in self.karte:
                if karta.barva!='H' and (karta.barva,karta.rank)!=('S','Q'):
                    nenegativne_karte.append(karta)
                if karta.barva=='C':
                    ustrezne_karte.append(karta)
                    ustrezne_vr.append(karta.vrednost_rank)
                #imam križevo dvojko
                if (karta.barva,karta.rank)==('C','2'):
                    krizeva_dvojka=karta
                    self.izbrana_karta=krizeva_dvojka
                #nimam križeve dvojke
                if self.ima_krizevo_dvojko()==False:
                    #če kart zahtevane barve nimamo
                    if len(ustrezne_karte)==0:
                        #v prvem krogu ne smemo metati kart, ki dajejo
                        #negativne točke
                        for karta in nenegativne_karte:
                            vr_neneg.append(karta.vrednost_rank)
                        min_vr=min(vr_neneg)
                        for karta in nenegativne_karte:
                            #znebimo se karte najmanjše vrednosti,
                            #ki ne daje negativnih točk
                            if karta.vrednost_rank==min_vr:
                                self.izbrana_karta=karta
                    #imamo vsaj eno karto zahtevane barve
                    if len(ustrezne_karte)>0:
                        max_vr=max(ustrezne_vr)
                        if len(self.igra.kup)==3:
                            self.izbrana_karta=Igralec.poberi_zadnji(ustrezne_karte,ustrezne_odlozene)
                        if len(self.igra.kup)<3:
                            self.izbrana_karta=Igralec.poberi(ustrezne_karte,ustrezne_odlozene)

            return self.izbrana_karta


        # prve karte se že bile odvržene - vsi igralci imajo 12 kart ali manj
        else:
            for karta in self.karte:
                vrednosti.append(karta.vrednost_rank)
            #krog začnem jaz (sem pobral kup prejšnji krog)
            if self.igra.kup == []:
                #dam karto največje vrednosti (upamo, da bomo pobrali)
                max_vr=max(vrednosti)
                for karta in self.karte:
                    if karta.vrednost_rank==max_vr:
                        self.izbrana_karta=karta
            else: #jaz ne začnem kroga
                prva_karta=self.igra.kup[0]
                for karta in self.karte:
                    vrednosti.append(karta.vrednost_rank)
                    if karta.barva==prva_karta.barva:
                        ustrezne_karte.append(karta)
                        ustrezne_vr.append(karta.vrednost_rank)
                    if karta.barva!='H' and (karta.barva,karta.rank)!=('S','Q'):
                        nesrceve_karte.append(karta)
                        nesrceve_vr.append(karta.vrednost_rank)
                for karta in self.igra.kup:
                    if karta.barva==prva_karta.barva:
                        ustrezne_odlozene.append(karta)
                if len(ustrezne_karte)==0:
                    max_vr1=max(vrednosti)
                    if len(nesrceve_karte)==0:
                        #nimam ustreznih kart, imam samo še srčeve karte
                        #ne bom pobral - ne morem več klatiti zvezd
                        #od sedaj najprej bom skušal pobrati čim manj - znebim se največje
                        for karta in self.karte:
                            if karta.vrednost_rank==max_vr1:
                                self.izbrana_karta=karta
                    else:
                        min_vr=min(nesrceve_vr)
                        #znebimo se karte najmanjše vrednosti
                        #(ampak ne srčevih - srca skušamo pobrati!)
                        for karta in nesrceve_karte:
                            if karta.vrednost_rank==min_vr:
                                self.izbrana_karta=karta
                #imamo vsaj eno karto zahtevane barve
                if len(ustrezne_karte)>0:
                    if len(self.igra.kup)==3:
                        self.izbrana_karta=Igralec.poberi_zadnji(ustrezne_karte,ustrezne_odlozene)
                    if len(self.igra.kup)!=3:
                        self.izbrana_karta=Igralec.poberi(ustrezne_karte,ustrezne_odlozene)

            return self.izbrana_karta

    def izberi_karto(self):
        asi=[]
        kralji=[]
        dame=[]
        visoke_pikove=[]
        visoke_srceve=[]

        prva_runda = (len(self.karte) == 13)
        for karta in self.karte:
            if karta.rank=='A':
                asi.append(karta)
            if karta.rank=='K':
                kralji.append(karta)
            if karta.rank=='Q':
                dame.append(karta)
            if karta.barva=='S' and (karta.rank=='Q' or karta.rank=='K' or karta.rank=='A'):
                visoke_pikove.append(karta)
            if karta.barva=='H' and (karta.rank=='Q' or karta.rank=='K' or karta.rank=='A'):
                visoke_srceve.append(karta)

        if len(self.karte)==13:
            if ((len(asi)>=2 and len(kralji)>=2 and len(dame)>=1) or
                (len(asi)>=1 and len(kralji)>=2 and len(dame)>=3) or
                (len(visoke_pikove)==3 or len(visoke_srceve)==3)):
                self.sklati_zvezde(prva_runda)
                self.nacin_igre=True

            else:
                self.poberi_cim_manj(prva_runda)
                self.nacin_igre=False

            return self.izbrana_karta

        else:
            if self.nacin_igre==True: #skušam sklatiti zvezde
                #dokler imajo vsi ostali igralci 0 točk - klatimo zvezde
                if self.ime=='Zahod':
                    if (self.igra.igralci[0].tocke1==0 and
                        self.igra.igralci[2].tocke1==0 and
                        self.igra.igralci[3].tocke1==0):
                        self.sklati_zvezde(prva_runda)
                        self.nacin_igre=True
                        return self.izbrana_karta
                    else:
                        #nekdo nam je uničil načrte in pobral vsaj eno vredno karto
                        self.poberi_cim_manj(prva_runda)
                        self.nacin_igre=False
                        return self.izbrana_karta
                if self.ime=='Sever':
                    if (self.igra.igralci[0].tocke1==0 and
                            self.igra.igralci[1].tocke1==0 and
                            self.igra.igralci[3].tocke1==0):
                        self.sklati_zvezde(prva_runda)
                        self.nacin_igre=True
                        return self.izbrana_karta
                    else:
                        self.poberi_cim_manj(prva_runda)
                        self.nacin_igre=False
                        return self.izbrana_karta
                if self.ime=='Vzhod':
                    if (self.igra.igralci[0].tocke1==0 and
                            self.igra.igralci[1].tocke1==0 and
                            self.igra.igralci[2].tocke1==0):
                        self.sklati_zvezde(prva_runda)
                        self.nacin_igre=True
                        return self.izbrana_karta
                    else:
                        self.poberi_cim_manj(prva_runda)
                        self.nacin_igre=False
                        return self.izbrana_karta

            if self.nacin_igre==False:
                self.poberi_cim_manj(prva_runda)
                return self.izbrana_karta

    def poteza(self):
        self.igra.after_id = None
        izbrana_karta=self.izberi_karto()
        self.karte.remove(izbrana_karta)
        self.odlozene.append(izbrana_karta)
        self.igra.kup.append(izbrana_karta)
        self.igra.vse_ze_polozene_karte.append(izbrana_karta)

        if self.ime == 'Zahod':
            izbrana_karta.zbrisi_karto(self.igra.canvas)
            izbrana_karta.narisi_karto(self.igra.canvas,self.igra.slovar_kart[(izbrana_karta.barva,izbrana_karta.rank)],320,260)
        if self.ime == 'Sever':
            izbrana_karta.zbrisi_karto(self.igra.canvas)
            izbrana_karta.narisi_karto(self.igra.canvas,self.igra.slovar_kart[(izbrana_karta.barva,izbrana_karta.rank)],394,210)
        if self.ime == 'Vzhod':
            izbrana_karta.zbrisi_karto(self.igra.canvas)
            izbrana_karta.narisi_karto(self.igra.canvas,self.igra.slovar_kart[(izbrana_karta.barva,izbrana_karta.rank)],470,260)

        self.igra.naslednji()


class Clovek(Igralec):
    def __init__(self,igra,ime=''):
        Igralec.__init__(self,igra,ime)

    def ustrezna(self,izbrana_karta,prva_runda=True):
        ustrezne_karte = []
        nenegativne_karte = []
        nesrceve_karte = []

        if len(self.karte)==13: #položena prva karta
            if self.ima_krizevo_dvojko()==True: #začnem jaz
                while (izbrana_karta.barva,izbrana_karta.rank) != ('C','2'):
                    return False
                return True
            if self.ima_krizevo_dvojko()==False: #ne začnem jaz
                for karta in self.karte:
                    if karta.barva=='C':
                        ustrezne_karte.append(karta)
                    if karta.barva!='H' and (karta.barva,karta.rank)!=('S','Q'):
                        nenegativne_karte.append(karta)
                    if karta.barva!='H':
                        nesrceve_karte.append(karta)
                if len(ustrezne_karte)==0:
                    while izbrana_karta.barva=='H':
                        if len(nesrceve_karte)>0:
                            return False
                        #skrajen primer - srce še ni odprto, a mi imamo
                        #samo srčeve karte
                        else:
                            return True
                    while (izbrana_karta.barva,izbrana_karta.rank)==('S','Q'):
                        if len(nenegativne_karte)>0:
                            return False
                        #skrajen primer - srce še ni odprto, a mi imamo
                        #samo srčeve karte in pikovo damo
                        else:
                            return True
                if len(ustrezne_karte)>0:
                    while izbrana_karta.barva!='C':
                        return False
                return True

        else: #prva karta je že bila položena
            #začnem jaz
            if self.igra.kup == []:
                srceve_karte=[] #že položene karte srčeve barve
                for karta in self.karte:
                    if karta.barva!='H':
                        nesrceve_karte.append(karta)
                for karta in self.igra.vse_ze_polozene_karte:
                    if karta.barva=='H':
                        srceve_karte.append(karta)
                while izbrana_karta.barva=='H':
                    #srce še ni odprto
                    if len(srceve_karte)==0:
                        if len(nesrceve_karte)>0:
                            return False
                        #skrajen primer - srce še ni odprto, a mi imamo
                        #samo srčeve karte
                        else:
                            return True
                    return True
                return True

            else:
                #ne ačnem jaz
                srceve_karte=[] #že položene karte srčeve barve
                for karta in self.igra.vse_ze_polozene_karte:
                    if karta.barva=='H':
                        srceve_karte.append(karta)
                for karta in self.karte:
                    if karta.barva==self.igra.kup[0].barva:
                        ustrezne_karte.append(karta)
                    if karta.barva!='H':
                        nesrceve_karte.append(karta)
                if len(ustrezne_karte)==0:
                    while izbrana_karta.barva=='H':
                        if len(srceve_karte)==0:
                            if len(nesrceve_karte)>0:
                                return False
                            #skrajen primer - srce še ni odprto, a mi imamo
                            #samo srčeve karte
                            else:
                                return True
                        return True
                if len(ustrezne_karte)>0:
                    while izbrana_karta.barva!=self.igra.kup[0].barva:
                        return False
                return True


    def poteza(self,event):
        if 31<=event.x<=len(self.karte)*56 + 31 + 10 and 405<=event.y<=495:
            # ugotovi, katero karto je kliknil
            index_karte=min((event.x - 31)//56, len(self.karte)-1)

            #če je karta ustrezna, izvedi potezo
            if self.ustrezna(self.karte[index_karte]):
                izbrana_karta=self.karte[index_karte]
                self.karte.pop(index_karte)

                for karta in self.karte[index_karte:]:
                    self.igra.canvas.move(karta.oznaka,-56,0)

                self.odlozene.append(izbrana_karta)
                self.igra.kup.append(izbrana_karta)
                self.igra.vse_ze_polozene_karte.append(izbrana_karta)

                self.igra.canvas.coords(izbrana_karta.oznaka,394,310)

                self.igra.naslednji()


class Srca(Frame):
    def __init__(self,root):

        self.root=root

        #ustvari igralce
        self.igralci=[
            Clovek(self,'Jug'),
            Racunalnik(self,'Zahod'),
            Racunalnik(self,'Sever'),
            Racunalnik(self,'Vzhod')
        ]

        self.na_potezi = None
        self.after_id = None

        #ustvari karte
        self.deck=Deck()

        self.kup=[]
        self.vse_ze_polozene_karte=[]

        # Kako dolgo "razmišlja" računalnik
        self.pavza = 1000

        #ustvar grafično ozadje
        menu=Menu(root)
        root.title('♥ SRCA ♥')
        root.config(menu=menu)


        igra_menu=Menu(menu)
        menu.add_cascade(label='Igra', menu=igra_menu)
        igra_menu.add_command(label='Nova igra', command=self.nova_igra)
        igra_menu.add_separator()
        igra_menu.add_command(label='Izhod', command=root.quit)
        

        okvir = Frame(root, borderwidt=5)
        okvir.grid(row=0, column=1, sticky='WNES')

        self.okvir_rezultat = Label(okvir,text='\nREZULTAT:',font='Tahoma 12 bold')
        self.okvir_rezultat.grid(stick='WE')

        for igralec in self.igralci:
            if igralec.stevilka==0:
                igralec.tck = IntVar()
                igralec.tck.set(igralec.tocke)
                igralec.napis_tocke = Label(okvir, text='\nJaz', font='Tahoma 12 bold').grid(stick='WE')
                igralec.podatek_tocke = Label(okvir, textvariable = igralec.tck, font='Tahoma', fg='red').grid(sticky='WE')
            if igralec.stevilka==1:
                igralec.tck = IntVar()
                igralec.tck.set(igralec.tocke)
                igralec.napis_tocke = Label(okvir, text='\nZahod', font='Tahoma 12 bold').grid(stick='WE')
                igralec.podatek_tocke = Label(okvir, textvariable = igralec.tck, font='Tahoma').grid(sticky='WE')
            if igralec.stevilka==2:
                igralec.tck = IntVar()
                igralec.tck.set(igralec.tocke)
                igralec.napis_tocke = Label(okvir, text='\nSever', font='Tahoma 12 bold').grid(stick='WE')
                igralec.podatek_tocke = Label(okvir, textvariable = igralec.tck, font='Tahoma').grid(sticky='WE')
            if igralec.stevilka==3:
                igralec.tck = IntVar()
                igralec.tck.set(igralec.tocke)
                igralec.napis_tocke = Label(okvir, text='\nVzhod', font='Tahoma 12 bold').grid(stick='WE')
                igralec.podatek_tocke = Label(okvir, textvariable = igralec.tck, font='Tahoma').grid(sticky='WE')

        self.okvir_komentar1 = Label(okvir,text='ZMAGALI\nSTE!\n\nČESTITAMO!\n',font='Tahoma 12 bold',fg='red')
        self.okvir_komentar2 = Label(okvir,text='Na žalost ste\nto igro\n izgubili.\n',font='Tahoma 12 bold')

        self.nova_igra_gumb = Button(okvir, text='Nova Igra',font='Tahoma 12 bold', command=self.nova_igra, state=DISABLED)
        self.nova_igra_gumb.grid(stick='WES',pady=30)

        self.canvas = Canvas(master, width=800, height=510, bg = 'dark green')
        self.canvas.grid(row = 0, column = 0)

        self.canvas.create_text(60,25,text='Zahod',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(390,25,text='Sever',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(740,25,text='Vzhod',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(390,385,text='Jaz',font='Tahoma 15 bold',fill='white')

        #slovar vseh kart - dostop do slik
        self.slovar_kart = {
            ('H', 'A'): PhotoImage(file='Slike/Karte/Srce/A.srce.gif'),
            ('H', '2'): PhotoImage(file='Slike/Karte/Srce/2.srce.gif'),
            ('H', '3'): PhotoImage(file='Slike/Karte/Srce/3.srce.gif'),
            ('H', '4'): PhotoImage(file='Slike/Karte/Srce/4.srce.gif'),
            ('H', '5'): PhotoImage(file='Slike/Karte/Srce/5.srce.gif'),
            ('H', '6'): PhotoImage(file='Slike/Karte/Srce/6.srce.gif'),
            ('H', '7'): PhotoImage(file='Slike/Karte/Srce/7.srce.gif'),
            ('H', '8'): PhotoImage(file='Slike/Karte/Srce/8.srce.gif'),
            ('H', '9'): PhotoImage(file='Slike/Karte/Srce/9.srce.gif'),
            ('H', 'T'): PhotoImage(file='Slike/Karte/Srce/T.srce.gif'),
            ('H', 'J'): PhotoImage(file='Slike/Karte/Srce/J.srce.gif'),
            ('H', 'Q'): PhotoImage(file='Slike/Karte/Srce/Q.srce.gif'),
            ('H', 'K'): PhotoImage(file='Slike/Karte/Srce/K.srce.gif'),
            ('S', 'A'): PhotoImage(file='Slike/Karte/Pik/A.pik.gif'),
            ('S', '2'): PhotoImage(file='Slike/Karte/Pik/2.pik.gif'),
            ('S', '3'): PhotoImage(file='Slike/Karte/Pik/3.pik.gif'),
            ('S', '4'): PhotoImage(file='Slike/Karte/Pik/4.pik.gif'),
            ('S', '5'): PhotoImage(file='Slike/Karte/Pik/5.pik.gif'),
            ('S', '6'): PhotoImage(file='Slike/Karte/Pik/6.pik.gif'),
            ('S', '7'): PhotoImage(file='Slike/Karte/Pik/7.pik.gif'),
            ('S', '8'): PhotoImage(file='Slike/Karte/Pik/8.pik.gif'),
            ('S', '9'): PhotoImage(file='Slike/Karte/Pik/9.pik.gif'),
            ('S', 'T'): PhotoImage(file='Slike/Karte/Pik/T.pik.gif'),
            ('S', 'J'): PhotoImage(file='Slike/Karte/Pik/J.pik.gif'),
            ('S', 'Q'): PhotoImage(file='Slike/Karte/Pik/Q.pik.gif'),
            ('S', 'K'): PhotoImage(file='Slike/Karte/Pik/K.pik.gif'),
            ('D', 'A'): PhotoImage(file='Slike/Karte/Kara/A.kara.gif'),
            ('D', '2'): PhotoImage(file='Slike/Karte/Kara/2.kara.gif'),
            ('D', '3'): PhotoImage(file='Slike/Karte/Kara/3.kara.gif'),
            ('D', '4'): PhotoImage(file='Slike/Karte/Kara/4.kara.gif'),
            ('D', '5'): PhotoImage(file='Slike/Karte/Kara/5.kara.gif'),
            ('D', '6'): PhotoImage(file='Slike/Karte/Kara/6.kara.gif'),
            ('D', '7'): PhotoImage(file='Slike/Karte/Kara/7.kara.gif'),
            ('D', '8'): PhotoImage(file='Slike/Karte/Kara/8.kara.gif'),
            ('D', '9'): PhotoImage(file='Slike/Karte/Kara/9.kara.gif'),
            ('D', 'T'): PhotoImage(file='Slike/Karte/Kara/T.kara.gif'),
            ('D', 'J'): PhotoImage(file='Slike/Karte/Kara/J.kara.gif'),
            ('D', 'Q'): PhotoImage(file='Slike/Karte/Kara/Q.kara.gif'),
            ('D', 'K'): PhotoImage(file='Slike/Karte/Kara/K.kara.gif'),
            ('C', 'A'): PhotoImage(file='Slike/Karte/Križ/A.križ.gif'),
            ('C', '2'): PhotoImage(file='Slike/Karte/Križ/2.križ.gif'),
            ('C', '3'): PhotoImage(file='Slike/Karte/Križ/3.križ.gif'),
            ('C', '4'): PhotoImage(file='Slike/Karte/Križ/4.križ.gif'),
            ('C', '5'): PhotoImage(file='Slike/Karte/Križ/5.križ.gif'),
            ('C', '6'): PhotoImage(file='Slike/Karte/Križ/6.križ.gif'),
            ('C', '7'): PhotoImage(file='Slike/Karte/Križ/7.križ.gif'),
            ('C', '8'): PhotoImage(file='Slike/Karte/Križ/8.križ.gif'),
            ('C', '9'): PhotoImage(file='Slike/Karte/Križ/9.križ.gif'),
            ('C', 'T'): PhotoImage(file='Slike/Karte/Križ/T.križ.gif'),
            ('C', 'J'): PhotoImage(file='Slike/Karte/Križ/J.križ.gif'),
            ('C', 'Q'): PhotoImage(file='Slike/Karte/Križ/Q.križ.gif'),
            ('C', 'K'): PhotoImage(file='Slike/Karte/Križ/K.križ.gif'),
            ('B', 'S'): PhotoImage(file='Slike/Karte/card_back_s.gif'), #Back Side = BS
            ('B', 'G'): PhotoImage(file='Slike/last_background.gif')} #BackGround = BG

    def nova_igra(self):
        self.nova_igra_gumb.config(state=DISABLED)
        if self.after_id is not None:
            self.canvas.after_cancel(self.after_id)

        #ponastavimo sezname
        for i in range(4):
            self.igralci[i].tocke1=0
            self.igralci[i].tocke=0

            self.igralci[i].odlozene=[]
            self.igralci[i].karte=[]

        self.kup=[]
        self.vse_ze_polozene_karte=[]

        self.posodobi_tocke()

        self.okvir_komentar1.grid_remove()
        self.okvir_komentar2.grid_remove()

        self.canvas.delete(ALL)

        self.canvas.create_text(60,25,text='Zahod',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(390,25,text='Sever',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(740,25,text='Vzhod',font='Tahoma 15 bold',fill='white')
        self.canvas.create_text(390,385,text='Jaz',font='Tahoma 15 bold',fill='white')

        self.nova_runda()

    def nova_runda(self):
        for i in range(4):
            self.igralci[i].odlozene=[]
            self.igralci[i].karte=[]
            self.igralci[i].tocke1=0
        self.izbrisi_kup()
        self.vse_ze_polozene_karte=[]
        self.razdeli_karte()
        self.nastavi_prvega()

    def razdeli_karte(self):
        self.deck.premesaj()
        for (i,karta) in enumerate(self.deck.karte):
            self.igralci[i%4].dodaj_karto(karta)
        self.jug=Deck.uredi(self.igralci[0])
        self.zahod=Deck.uredi(self.igralci[1])
        self.sever=Deck.uredi(self.igralci[2])
        self.vzhod=Deck.uredi(self.igralci[3])
        for (j,karta) in enumerate(self.jug):
            karta.narisi_karto(self.canvas,self.slovar_kart[(karta.barva,karta.rank)],j*56+64,450)
        for (j,karta) in enumerate(self.zahod):
             karta.narisi_karto(self.canvas,self.slovar_kart[('B','S')],60,j*20+90)
        for (j,karta) in enumerate(self.sever):
              karta.narisi_karto(self.canvas,self.slovar_kart[('B','S')],j*30+220,90)
        for (j,karta) in enumerate(self.vzhod):
             karta.narisi_karto(self.canvas,self.slovar_kart[('B','S')],740,j*20+90)


    def nastavi_prvega(self):
        #poišče igralca križevo dvojko
        for igralec in self.igralci:
            if igralec.ima_krizevo_dvojko():
                self.nastavi_na_potezi(igralec)
                return
        print ("ZGODILA SE JE GROZNA NAPAKA - NIHČE NI NA POTEZI")


    def nastavi_na_potezi(self, igralec):
        """Nastavi vse, da bo dani igralec na potezi. Če je igralec None, je treba razdeliti karte in narediti novo rundo."""
        self.canvas.unbind('<Button-1>')
        self.na_potezi = igralec
        if igralec is None:
            # Runde je konec, razdeli karte
            self.nova_runda()
        if isinstance(igralec, Clovek):
            # na potezi je clovek
            self.canvas.bind('<Button-1>', igralec.poteza)
        else:
            # na potezi je racunalnik
            self.after_id = self.canvas.after(self.pavza, igralec.poteza)

    def izbrisi_kup(self):
        for karta in self.kup:
            karta.zbrisi_karto(self.canvas)
        self.kup=[]
            

    def naslednji(self):
        """Naredi vse, da bo na potezi naslednji, ali pa končaj igro."""
        if len(self.vse_ze_polozene_karte) == 52:
            print ("===== KONEC RUNDE =====")
            # konec runde
            # popravi tocke
            self.kdo_pobere()
            for igralec in self.igralci:
                if igralec.tocke1 != 26:
                    igralec.tocke += igralec.tocke1
                else:
                    #igralec je "Sklatil Zvezde"
                    for igr in self.igralci:
                        if igr != igralec: igr.tocke += 26

            self.posodobi_tocke()
            # ali je konec igre?
            if (self.igralci[0].tocke<100 and
                self.igralci[1].tocke<100 and
                self.igralci[2].tocke<100 and
                self.igralci[3].tocke<100):
                # zacni novo rundo
                self.canvas.after(self.pavza, self.nova_runda)
            else:
                self.posodobi_tocke()
                # igre je konec
                self.koncaj_igro()
        else:
            if len(self.kup) == 4:
                # konec kroga
                self.canvas.after(self.pavza, self.izbrisi_kup)
                k = self.kdo_pobere()
                self.nastavi_na_potezi(self.igralci[k])
            else:
                k = (self.na_potezi.stevilka + 1) % 4
                self.nastavi_na_potezi(self.igralci[k])


    def kdo_pobere(self): #poišče igralca, ki je nazadnje pobral karte + prešteje točke
        tocke=[] #karte ustrezne barva
        tocke_vr=[] #točke kart ustrezne barve
        vsota=0
        for karta in self.kup:
            if karta.barva=='H':
                vsota+=1
            if (karta.barva,karta.rank)==('S','Q'):
                vsota+=13
            if karta.barva==self.kup[0].barva:
                tocke.append(karta)
                tocke_vr.append(karta.vrednost_rank)

        max_vr=tocke_vr.index(max(tocke_vr)) #index karte najvecje vrednosti
        karta_max=tocke[max_vr] #karta največje vrednosti

        for igralec in self.igralci:
            if karta_max in igralec.odlozene:
                igralec.tocke1+=vsota
                return igralec.stevilka


    def posodobi_tocke(self):
        #posodobi točke igralcev - za izpis
        for igralec in self.igralci:
            igralec.tck.set(igralec.tocke)


    def koncaj_igro(self):
        #izpišemo izid igre
        self.canvas.delete(ALL)
        self.canvas.create_image(400,260,image=self.slovar_kart[('B','G')])
        clovek = self.igralci[0]
        # povej uporabniku, ali je zmagal ali zgubil
        if clovek.tocke <= min(self.igralci[1].tocke, self.igralci[2].tocke, self.igralci[3].tocke):
            self.okvir_komentar1.grid(stick='WE')
        else:
            self.okvir_komentar2.grid(stick='WE')

        self.nova_igra_gumb.config(state=ACTIVE)


master=Tk()
main=Srca(master)
main.nova_igra()
master.mainloop()

