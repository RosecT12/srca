![Hearts-Free11.jpg](https://bitbucket.org/repo/gApAop/images/480646430-Hearts-Free11.jpg)

### Čemu je namenjen ta repozitorij? ###

* Izdelala bom igrico Srca

### Kako zagnati program? ###

* Program na običajen način poženete z datoteko Srca.py.
* Kadarkoli med igro lahko zaključite igro.

### Navodila ###
* Cilj: Zbrati čim manj točk.
* Igralna miza: Igra Srca se igra z enim kompletom 52 kart. Vaši nasprotniki (vsi so računalnik) so Zahod, Sever in Vzhod. Vsak igralec prejme 13 kart.
* Pravila igre: Igralec s križevo dvojko začne prvi krog. Igralci morajo vreči karto iste barve. Če nimate karte te barve, lahko vržete katero koli drugo karto (razen v prvem krogu, ko igralec ne sme vreči srca ali pikove dame). Kdor ima najvišjo karto, pobere vzetek in začne naslednji krog. V igri Srca so karte razvrščene od asa (najvišja karta) do dvojke (najnižja karta). Igralci lahko začnejo naslednje kroge s karto v kateri koli barvi. Izjema so srca. Srca ni dovoljeno vreči, če ga ni že drug igralec vrgel v prejšnjem krogu. (Ali če uporabimo žargonski izraz, dokler srca niso odprta.)
Cilj igre Srca je podati vsa srca drugim igralcem (ki pa skušajo svoja srca podati vam). Igra se konča, ko kdo od igralcev doseže 100 točk. V tem trenutku zmaga samo igralec z najnižjim številom skupnih točk, ostali izgubijo.
* Točkovanje: Vsaka srčeva karta v vzetku je vredna 1 točko. Pikova dama je vredna 13 točk.
* Sklatiti zvezde: Igralec, ki v igri Srca »doseže vse možne točke«, zbere vsa razpoložljiva srca in pikovo damo. Nasprotniki tako dobijo 26 točk. Rezultat igralca, ki sklati zvezde, ostane nespremenjen.

###Koga kontaktirati za dodatne informacije? ###

* Talita Rosec (talita.rosec@student.fmf.uni-lj.si)